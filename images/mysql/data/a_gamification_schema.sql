-- -----------------------------------------------------
-- Schema Gamification
-- -----------------------------------------------------
USE `gamification`;

-- -----------------------------------------------------
-- Table user
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gamification`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `is_active` TINYINT NOT NULL DEFAULT 1,
  `is_verified` TINYINT NOT NULL DEFAULT 0,
  `password_reseted` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table role
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gamification`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table user_has_role
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gamification`.`user_has_role` (
  `user_id` INT NOT NULL,
  `role_id` INT NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  CONSTRAINT `fk_user_has_role_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `gamification`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_has_role_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `gamification`.`role` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table application
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gamification`.`application` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `api_key` VARCHAR(100) NOT NULL,
  `api_secret` VARCHAR(100) NOT NULL,
  `description` VARCHAR(255) NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_application_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `gamification`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table validation
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gamification`.`validation` (
  `user_id` INT NOT NULL,
  `token` VARCHAR(250) NOT NULL,
  `validity` DATE NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_validation_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `gamification`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
