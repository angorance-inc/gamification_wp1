-- -----------------------------------------------------
-- Schema Gamification
-- -----------------------------------------------------
USE `gamification`;

-- -----------------------------------------------------
-- Table user
-- -----------------------------------------------------
INSERT INTO `gamification`.`user` (`id`, `first_name`, `last_name`, `email`, `password`, `is_active`, `is_verified`) 
VALUES (1, "admin", "admin", "iamtheboss@admin.ch", "$2a$10$dKxjmogUHENYn/FReT9RE.wLq1APMir0FoeXBqOTaqWXDj35F2mkC", true, true); -- Password is the hash of "P@ssw0rd"

-- -----------------------------------------------------
-- Table role
-- -----------------------------------------------------
INSERT INTO `gamification`.`role` (`id`, `name`) VALUES (1, "Administrator");
INSERT INTO `gamification`.`role` (`id`, `name`) VALUES (2, "Developper");

-- -----------------------------------------------------
-- Table user_has_role
-- -----------------------------------------------------
INSERT INTO `gamification`.`user_has_role` (`user_id`, `role_id`) VALUES (1, 1);
