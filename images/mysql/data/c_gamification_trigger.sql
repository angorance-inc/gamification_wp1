USE `gamification`;

/******* Trigger set default role *******/
DELIMITER $$
CREATE TRIGGER set_default_role
    AFTER INSERT 
    ON `gamification`.`user` FOR EACH ROW 
BEGIN
	DECLARE idDevelopperRole LONG;
    
	/* Find the id of the developper role */
    SELECT id INTO idDevelopperRole
    FROM `gamification`.`role`
    WHERE name = 'Developper';

	/* Add the developper role to the new user */
    INSERT INTO `gamification`.`user_has_role` (user_id, role_id)
    VALUES (NEW.id, idDevelopperRole);
END; $$
DELIMITER ;