package ch.heigvd.amt.mvcsimple.business;

import ch.heigvd.amt.mvcsimple.services.dao.UsersManagerLocal;
import ch.heigvd.amt.mvcsimple.smtp.MailSender;
import org.apache.commons.lang3.RandomStringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Gonzalez Lopez
 * @version 1.0
 */
public class AdministrationManagement {

	/**
	 * Reset the password of a user and send it by email.
	 *
	 * @param usersManager
	 * @param mailSender
	 * @param request
	 */
	public void handleResetPasswordUser(UsersManagerLocal usersManager, MailSender mailSender, HttpServletRequest request) {
		
		List<String> errors = new ArrayList<>();
		
		// Get user id from the request
		long id = Long.parseLong(request.getParameter("userId"));
		String email = request.getParameter("email");

		// Generate a new password
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()-_";
		String newPassword = RandomStringUtils.random( 10, characters );

		// Reset password in database
		if (usersManager.resetPassword(id, newPassword, true)) {

			// Send email to the user with the new password
			if (mailSender.sendMail(email, "Password Reset", body(newPassword))) {

				request.setAttribute("info_success", "Password has been updated and email sent.");

			} else {
				errors.add("Email could not be sent to the user!!! User can't connect anymore!!!");
			}
		} else {
			errors.add("Impossible to update user's password at the moment");
		}
		
		if (!errors.isEmpty()) {
			request.setAttribute("info_errors", errors);
		}
	}
	
	/**
	 * Create the body of the email with the new generated password
	 *
	 * @param password  New password
	 *
	 * @return The email's body
	 */
	private String body(String password) {
		
		return "Hi there,<br /><br />Your password had to be reset for some security concerns.<br />"
				+ "Use the following password to connect to your account and change it: "
				+ password + "<br /><br />"
				+ "Thank you for your understanding and have a nice Game, <br /><br />"
				+ "Your Gamification's Team";
	}
	
	public void handleToggleActivity(UsersManagerLocal usersManager, HttpServletRequest request) {
		
		// Get user's data
		long id = Long.parseLong(request.getParameter("userId"));
		boolean isActive = Boolean.parseBoolean(request.getParameter("isActive"));
		
		String currentState = isActive ? " deactivated" : " activated";
		
		if (usersManager.toggleActiveUser(id, !isActive)) {
			
			request.setAttribute("info_success", "The user was " + currentState);
		} else {
			
			request.setAttribute("info_errors", "The user couldn't be " + currentState);
		}
	}
}
