package ch.heigvd.amt.mvcsimple.business;

import ch.heigvd.amt.mvcsimple.services.dao.*;
import ch.heigvd.amt.mvcsimple.model.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;

public class GamificationManagement {

    /**
     * Create an application for the current user.
     *
     * @param request       Get request sent by the user.
     *
     * @throws ServletException
     * @throws IOException
     *
     * @return true if it worked, false otherwise
     */
    public boolean handleCreateAppOperation(long user_id, ApplicationsManagerLocal applicationsManager, HttpServletRequest request) {

        // Get all application information from the request
        String name = request.getParameter("newApp_Name");
        String description = request.getParameter("newApp_Description");

        // Create the application
        String api_key = generateAPIKeyOrSecret();
        String api_secret = generateAPIKeyOrSecret();

        // Check if the app exists
        if(!applicationsManager.isExistingApplication(0, name, user_id, false)) {

            // Create the app
            if(applicationsManager.createApplication(name, api_key, api_secret, description, user_id)){
                request.setAttribute("info_success", "Application has been created.");
                return true;

            } else {
                request.setAttribute("info_errors", "The application creation has failed.");
            }

        } else {
            request.setAttribute("info_errors", "An application already exists with this name.");
        }

        return false;
    }

    /**
     * Update an application of the current user.
     *
     * @param request       Get request sent by the user.
     *
     * @throws ServletException
     * @throws IOException
     */
    public void handleUpdateAppOperation(long user_id, ApplicationsManagerLocal applicationsManager, HttpServletRequest request) {

        // Get all application information from the request
        long id = Long.parseLong(request.getParameter("updateApp_Id"));
        String name = request.getParameter("updateApp_Name");
        String description = request.getParameter("updateApp_Description");

        // Check if the app exists
        if(!applicationsManager.isExistingApplication(id, name, user_id, true)) {

            // Update the application
            if(applicationsManager.updateApplication(id, name, description, user_id)){
                request.setAttribute("info_success", "Application has been updated.");

            } else {
                request.setAttribute("info_errors", "The application update has failed.");
            }
        } else {
            request.setAttribute("info_errors", "An application already exists with this name.");
        }
    }

    /**
     * Remove an application of the current user.
     *
     * @param request       Get request sent by the user.
     *
     * @throws ServletException
     * @throws IOException
     *
     * @return true if it worked, false otherwise
     */
    public boolean handleRemoveAppOperation(ApplicationsManagerLocal applicationsManager, HttpServletRequest request) {

        // Get all application information from the request
        long app_id = Long.parseLong(request.getParameter("removeApp_id"));

        // Remove the application
        if(!applicationsManager.removeApplication(app_id)){
            request.setAttribute("info_errors", "The application removal has failed, try again");
            return false;

        } else {
            request.setAttribute("info_success", "Application has been removed");
            return true;
        }
    }

    /**
     * Update the profile of the current user.
     *
     * @param user    Current user.
     * @param usersManager
     * @param request
     */
    public void handleUpdateUserOperation(User user, UsersManagerLocal usersManager, HttpServletRequest request) {

        // Get all user information from the request
        String firstName = request.getParameter("updateUser_FirstName");
        String lastName = request.getParameter("updateUser_LastName");

        // Update the user
        if(!usersManager.updateUser(user.getId(), firstName, lastName, null)){
            request.setAttribute("info_errors", "Your profile update has failed.");

        } else {
            request.setAttribute("info_success", "Your profile has been updated.");

            user.setFirstName(firstName);
            user.setLastName(lastName);
        }
    }

    /**
     * Update the password of the current user.
     *
     * @param user    Current user.
     * @param usersManager
     * @param request
     */
    public void handleUpdatePasswordOperation(User user, UsersManagerLocal usersManager, HttpServletRequest request) {

        // Get all user information from the request
        String newPassword = request.getParameter("updatePassword");

        // Check if the user has to change his password
        if (user.passwordIsReseted()) {

            // Update the password and remove the flag isReseted
            if (!usersManager.resetPassword(user.getId(), newPassword, false)) {
                request.setAttribute("info_errors", "Your password change has failed.");

            } else {
                request.setAttribute("info_success", "Your password has been changed.");
            }
        } else {

            // Update the password
            if (!usersManager.updateUser(user.getId(), user.getFirstName(), user.getLastName(), newPassword)) {
                request.setAttribute("info_errors", "Your password change has failed.");

            } else {
                request.setAttribute("info_success", "Your password has been changed.");
            }
        }
    }

    /**
     * Generate a random unique key or secret.
     *
     * @return key or secret generated.
     */
    public String generateAPIKeyOrSecret() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
