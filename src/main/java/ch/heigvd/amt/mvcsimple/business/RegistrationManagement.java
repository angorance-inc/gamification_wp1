package ch.heigvd.amt.mvcsimple.business;

import ch.heigvd.amt.mvcsimple.model.User;
import ch.heigvd.amt.mvcsimple.services.dao.*;
import ch.heigvd.amt.mvcsimple.smtp.MailSender;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;
import java.util.regex.*;

public class RegistrationManagement {

    private ValidationManagement validationService = new ValidationManagement();

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * Try to connect the current user.
     *
     * If the user connected is an administrator, he has to be redirected
     * on the administration side, otherwise on the gamification side.
     *
     * @param request       Get request sent by the user.
     * @param response      Response to return to the user.
     *
     * @throws ServletException
     * @throws IOException
     */
    public void handleSignInOperation(UsersManagerLocal usersManager, ValidationsManagerLocal validationsManager, MailSender mailSender, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get all user's information from the request
        String firstName = request.getParameter("signIn_FirstName");
        String lastName = request.getParameter("signIn_LastName");
        String email = request.getParameter("signIn_Email");
        String password = request.getParameter("signIn_Password");

        // Check fields
        boolean isCorrect = true;
        List<String> errors = new ArrayList<>();

        if(firstName.isEmpty()) {
            isCorrect = false;
            errors.add("First name is missing");

        } else {
            request.setAttribute("signIn_FirstName", firstName);
        }

        if(lastName.isEmpty()) {
            isCorrect = false;
            errors.add("Last name is missing");

        } else {
            request.setAttribute("signIn_LastName", lastName);
        }

        if(email.isEmpty() || !isCorrectEmail(email)) {
            isCorrect = false;
            errors.add("Email is missing or incorrect");

        } else {
            request.setAttribute("signIn_Email", email);
        }

        if(password.isEmpty() || password.length() < 8) {
            isCorrect = false;
            errors.add("Password is missing or not long enough (8 characters needed)");

        } else {
            request.setAttribute("signIn_Password", password);
        }


        // Show error message or redirect user on the good page for his role
        if(isCorrect) {

            long user_id;

            // Create the user
            if((user_id = usersManager.createUser(firstName, lastName, email, password)) != 0) {

                // Create the token
                String token = validationsManager.createValidationToken(user_id);

                if(token != null) {

                    // Send validation link
                    if(validationService.sendValidationEmail(firstName, lastName, email, token, mailSender, request)) {
                        request.setAttribute("info_success", "Your account has been created. Check your email to activate your account.");

                    } else {
                        errors.add("Your account has been created, but the email hasn't been sent. Contact the administrator.");
                    }

                } else {
                    errors.add("Your account has been created, but the token hasn't been generated. Try to log in to get a new one.");
                }

                // Empty the fields
                request.setAttribute("signIn_FirstName", "");
                request.setAttribute("signIn_LastName", "");
                request.setAttribute("signIn_Email", "");
                request.setAttribute("signIn_Password", "");

            } else {
                errors.add("The user's creation has failed or email address is already used for another account.");
            }
        }

        request.setAttribute("info_errors", errors);
        request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
    }

    /**
     * Try to connect the current user.
     *
     * If the user connected is an administrator, he has to be redirected
     * on the administration side, otherwise on the gamification side.
     *
     * @param request       Get request sent by the user.
     * @param response      Response to return to the user.
     *
     * @throws ServletException
     * @throws IOException
     */
    public void handleLogInOperation(UsersManagerLocal usersManager, ValidationsManagerLocal validationsManager, MailSender mailSender, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get all user's information from the request
        String email = request.getParameter("logIn_Email");
        String password = request.getParameter("logIn_Password");

        // Check fields
        boolean isCorrect = true;
        List<String> errors = new ArrayList<>();

        if(email.isEmpty()) {
            isCorrect = false;
            errors.add("Email is missing or incorrect");

        } else {
            request.setAttribute("logIn_Email", email);
        }

        if(password.isEmpty()) {
            isCorrect = false;
            errors.add("Password is missing");

        } else {
            request.setAttribute("logIn_Password", password);
        }

        // Check if the accounts exists and retrieve it if possible
        if(isCorrect) {

            User user;

            if((user = usersManager.getExistingUser(email, password)) != null) {

                // Redirect the user to the correct url
                if(user.isAdmin()) {

                    // Give the user to the next servlet
                    request.getSession().setAttribute("user", user);

                    request.getRequestDispatcher("/WEB-INF/pages/administration.jsp");
                    response.sendRedirect(request.getContextPath() + "/administration");

                } else {

                    // Check if the account has been verified
                    if(user.isVerified()) {

                        // Check if the account has been suspended
                        if(user.isActive()) {

                            // Give the user to the next servlet
                            request.getSession().setAttribute("user", user);

                            request.getRequestDispatcher("/WEB-INF/pages/gamification.jsp");
                            response.sendRedirect(request.getContextPath() + "/gamification");

                        } else {
                            request.setAttribute("info_errors", "This account has been suspended by the administrator. Contact him for more information.");
                            request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
                        }

                    } else {

                        // Create the token
                        String token = validationsManager.createValidationToken(user.getId());

                        if(token != null) {

                            // Send validation link
                            if(validationService.sendValidationEmail(user.getFirstName(), user.getLastName(), email, token, mailSender, request)) {
                                request.setAttribute("info_success", "Check your email to activate your account. A new validation link has been sent to you.");
                                request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);

                            } else {
                                request.setAttribute("info_errors", "The email with the new validation link hasn't been sent. Contact the administrator.");
                                request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
                            }

                        } else {
                            request.setAttribute("info_errors", "Your account has been created, but the token hasn't been generated. Try to log in to get a new one.");
                            request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
                        }
                    }
                }

            } else {
                request.setAttribute("info_errors", "This account doesn't exist or is wrong.");
                request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("info_errors", errors);
            request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
        }
    }

    /**
     * Check email with regex
     *
     * @param email email entered
     * @return  true if it's an email, false otherwise
     */
    public static boolean isCorrectEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.find();
    }
}
