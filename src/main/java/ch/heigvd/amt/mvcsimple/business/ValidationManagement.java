package ch.heigvd.amt.mvcsimple.business;

import ch.heigvd.amt.mvcsimple.smtp.MailSender;
import javax.servlet.http.*;

public class ValidationManagement {

    /**
     * Send an email to the current user which contains the validation link.
     *
     * @param firstName User's first name
     * @param lastName  User's last name
     * @param email User's email
     *
     * return true if sent, false otherwise
     */
    public boolean sendValidationEmail(String firstName, String lastName, String email, String token, MailSender mailSender, HttpServletRequest request) {

        // Prepare validation link
        String scheme = request.getScheme() + "://";
        String serverName = request.getServerName();
        String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
        String contextPath = request.getContextPath();
        String link = scheme + serverName + serverPort + contextPath + "/validation?token=" + token;

        String title = "Activation link for your Gamification account";

        String text = "Welcome to Gamification " + firstName + " " + lastName + ", <br /><br />"
                + "To use this application you need to activate your account by clicking this link: <br>"
                + link + "<br /><br />"
                + "Thanks for using our services <br /><br />" + "Your Gamification's Team";

        // Send the email
        return mailSender.sendMail(email, title, text);
    }
}
