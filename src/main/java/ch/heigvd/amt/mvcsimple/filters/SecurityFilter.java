package ch.heigvd.amt.mvcsimple.filters;

import ch.heigvd.amt.mvcsimple.model.User;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * This class implements the Filter interface defined in the Servlet API. A
 * filter is a component that is placed in the (HTTP) request processing
 * pipeline. It can inspect and manipulate both the request (on the way in) and
 * the response (on the way out).
 *
 * The responsibility of this class is to handle authorization of HTTP requests
 * issued by clients. The security policy is defined as follows: - all pages of
 * the application are protected and can be accessed only if the the user has
 * successfully authenticated. If that is the case, then there must be an object
 * named "principal" stored in the HTTP session. - static content (css,
 * javascript, etc.) is not protected - the login page, which displays the login
 * form, is not protected - the others servlet are not protected either
 *
 * These rules are enforced in the method. Note that there is no actual password
 * verification. Any password will be accepted in this illustrative scenario.
 *
 * @author Olivier Liechti (olivier.liechti@heig-vd.ch)
 */
public class SecurityFilter implements Filter {

    private String encoding;
    private FilterConfig filterConfig;

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        request.setCharacterEncoding(encoding);
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());

        /*
         * Let's apply a white list access policy. By default, we will authorize access to the target URI on
         * if the user has been authenticated.
         */
        boolean isTargetUrlProtected = true;

        /*
         * If the target URL is static content or if it is the registration servlet, then we grant access event
         * if the user has not been authenticated.
         */
        if (path.startsWith("/static/")) {
            isTargetUrlProtected = false;

        } else if ("/registration".equals(path)) {
            isTargetUrlProtected = false;

        } else if ("/validation".equals(path)) {
            isTargetUrlProtected = false;

        } else {
            /*
             * Let's imagine that the user has sent a request to /MVCDemo/pages/beers before logging into the
             * application. In that case, we want to route the user to the login page. If he provides valid
             * credentials, then we then want to redirect the user to /MVCDemo/pages/beers. In order to do that,
             * we need to save the target URL
             */
            request.setAttribute("targetUrl", path);
        }

        // If the user has been authenticated before, then the AuthenticationServlet has placed
        // an object (in this case a User) in the HTTP session. We can retrieve it.

        User user = (User) httpRequest.getSession().getAttribute("user");

        if (user == null && isTargetUrlProtected) {
            // If the user has not been authenticated ===> /registration
            httpResponse.sendRedirect(httpRequest.getContextPath() + "/registration");

        } else if(user != null) {

            if (!user.isAdmin() && ("/administration".equals(path) || "/registration".equals(path) || path.contains("/gamification/"))){
                // If the user is a dev and the page is /administration or /registration or /gamification/* ===> /gamification
                httpResponse.sendRedirect(httpRequest.getContextPath() + "/gamification");

            } else if(user.isAdmin() && "/registration".equals(path)) {
                // If the user is an admin and the page is /registration ===> /administration
                httpResponse.sendRedirect(httpRequest.getContextPath() + "/administration");
            }
            else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        this.encoding = filterConfig.getInitParameter("encoding");
    }

    @Override
    public void destroy() {
    }

}