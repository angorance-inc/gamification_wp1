package ch.heigvd.amt.mvcsimple.model;

public class Application {

    private long id;
    private String name;
    private String api_key;
    private String api_secret;
    private String description;
    private long user_id;

    public Application(long id, String name, String api_key, String api_secret, String description, long user_id) {
        this.id = id;
        this.name = name;
        this.api_key = api_key;
        this.api_secret = api_secret;
        this.description = description;
        this.user_id = user_id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getApiKey() {
        return api_key;
    }

    public String getApiSecret() {
        return api_secret;
    }

    public String getDescription() {
        return description;
    }

    public long getUserId() {
        return user_id;
    }
}
