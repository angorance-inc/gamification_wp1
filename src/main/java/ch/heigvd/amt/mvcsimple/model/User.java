package ch.heigvd.amt.mvcsimple.model;

public class User {

    private long id;
    private String first_name;
    private String last_name;
    private String email;
    private boolean is_active;
    private boolean is_verified;
    private boolean password_reseted;
    private boolean is_admin;
    private int nbrApps;

    public User(long id, String first_name, String last_name, String email, boolean is_active, boolean is_verified, boolean password_reseted, boolean is_admin, int nbrApps) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.is_active = is_active;
        this.is_verified = is_verified;
        this.password_reseted = password_reseted;
        this.is_admin = is_admin;
        this.nbrApps = nbrApps;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return is_active;
    }

    public boolean isVerified() {
        return is_verified;
    }

    public boolean passwordIsReseted() {
        return  password_reseted;
    }

    public boolean isAdmin() {
        return is_admin;
    }

    public int getNbrApps() {
        return nbrApps;
    }

    public void addApp() {
        ++nbrApps;
    }

    public void removeApp() {
        --nbrApps;
    }

    public void setFirstName(String firstName) {
        this.first_name = firstName;
    }

    public void setLastName(String lastName) {
        this.last_name = lastName;
    }

    public void setPasswordReseted(boolean isReseted) {
        this.password_reseted = isReseted;
    }
}
