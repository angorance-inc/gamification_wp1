package ch.heigvd.amt.mvcsimple.presentation;

import ch.heigvd.amt.mvcsimple.business.AdministrationManagement;
import ch.heigvd.amt.mvcsimple.model.User;
import ch.heigvd.amt.mvcsimple.services.dao.*;
import ch.heigvd.amt.mvcsimple.smtp.MailSender;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AdministrationServlet extends javax.servlet.http.HttpServlet {
    
    @EJB
    private UsersManagerLocal usersManager;
	
	@EJB
	private MailSender mailSender;
    
    private AdministrationManagement service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        service = new AdministrationManagement();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doSpecialGet(1, request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	    // Identify the form sent
	    String formName = request.getParameter("fFormName");
        int page = Integer.parseInt(request.getParameter("page"));

	    // Handle the request
	    if (formName.compareTo("regenUserPassword") == 0){
		    service.handleResetPasswordUser(usersManager, mailSender, request);

	    } else if (formName.compareTo("DisplayItems") == 0) {
            // do nothing, just retrieve the current page and refresh the page
            
        } else if (formName.compareTo("toggleActivity") == 0) {
	    	service.handleToggleActivity(usersManager, request);
	    }
	
	    // Redirect the user on the same page
        doSpecialGet(page, request, response);
    }

    private void doSpecialGet(int page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get the current user
        User authenticatedUser = (User) request.getSession().getAttribute("user");

        // Get all users
        int limitPerPage = 15;

        List<User> model = usersManager.getUsers(authenticatedUser.getId(), page, limitPerPage);
        request.setAttribute("users", model);

        // Set information for pagination
        request.setAttribute("nbrItems", usersManager.getNbrUsers() - 1);
        request.setAttribute("page", page);
        request.setAttribute("limitPerPage", limitPerPage);

        request.getRequestDispatcher("/WEB-INF/pages/administration.jsp").forward(request, response);
    }
}
