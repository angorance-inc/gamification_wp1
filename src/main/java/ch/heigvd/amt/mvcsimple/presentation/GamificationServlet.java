package ch.heigvd.amt.mvcsimple.presentation;

import ch.heigvd.amt.mvcsimple.model.*;
import ch.heigvd.amt.mvcsimple.business.GamificationManagement;
import ch.heigvd.amt.mvcsimple.services.dao.*;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

public class GamificationServlet extends HttpServlet {

    @EJB
    private ApplicationsManagerLocal applicationsManager;

    @EJB
    private UsersManagerLocal usersManager;

    private GamificationManagement service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        service = new GamificationManagement();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doSpecialGet(1, request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get the current user
        User authenticatedUser = (User) request.getSession().getAttribute("user");

        // Identify the form sent
        String formName = request.getParameter("fFormName");
        int page = 1;

        // Handle the request
        if(formName.compareTo("CreateApp") == 0){
            if(service.handleCreateAppOperation(authenticatedUser.getId(), applicationsManager, request)) {
                authenticatedUser.addApp();
            }

        } else if(formName.compareTo("UpdateApp") == 0) {
            service.handleUpdateAppOperation(authenticatedUser.getId(), applicationsManager, request);
            page = Integer.parseInt(request.getParameter("page"));

        } else if(formName.compareTo("RemoveApp") == 0) {
            if(service.handleRemoveAppOperation(applicationsManager, request)) {
                authenticatedUser.removeApp();
            }

            page = Integer.parseInt(request.getParameter("page"));

            // Go to the previous page, because the current is empty
            if(authenticatedUser.getNbrApps() % 15 == 0 && page != 1) {
                page -= 1;
            }

        } else if(formName.compareTo("DisplayItems") == 0) {
            page = Integer.parseInt(request.getParameter("page"));

        } else if(formName.compareTo("UpdateUser") == 0) {
            service.handleUpdateUserOperation(authenticatedUser, usersManager, request);

        } else if(formName.compareTo("UpdatePassword") == 0) {
            service.handleUpdatePasswordOperation(authenticatedUser, usersManager, request);

            if(authenticatedUser.passwordIsReseted()) {
                authenticatedUser.setPasswordReseted(false);
            }
        }

        // Redirect the user on the same page
        doSpecialGet(page, request, response);
    }

    private void doSpecialGet(int page, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Get the current user
        User authenticatedUser = (User) request.getSession().getAttribute("user");

        // Choose the owner of applications to display
        User user = authenticatedUser;

        if(request.getPathInfo() != null) {

            // If the path is /gamification/id
            try {

                // Get the id of the user to display
                long user_id = Long.parseLong(request.getPathInfo().replace("/", ""));

                // Get the user of the id
                user = usersManager.getUserByID(user_id);

                if(user == null) {
                    request.setAttribute("info_errors", "Link for user's applications is not valid. ID doesn't exist.");
                    request.getRequestDispatcher("/WEB-INF/pages/administration.jsp").forward(request, response);
                }

                // Set information to indicate that the admin is seeing another account than his own
                request.setAttribute("userToDisplay", user);
                request.setAttribute("pathInfo", request.getPathInfo());

            } catch (Exception e) {
                request.setAttribute("info_errors", "URL not valid");
                request.getRequestDispatcher("/WEB-INF/pages/administration.jsp").forward(request, response);
            }
        }

        // Get all applications of the user
        int limitPerPage = 15;

        List<Application> model = applicationsManager.getApplications(user.getId(), page, limitPerPage);
        request.setAttribute("applications", model);

        // Set information for pagination
        request.setAttribute("nbrItems", user.getNbrApps());
        request.setAttribute("page", page);
        request.setAttribute("limitPerPage", limitPerPage);

        request.getRequestDispatcher("/WEB-INF/pages/gamification.jsp").forward(request, response);
    }
}
