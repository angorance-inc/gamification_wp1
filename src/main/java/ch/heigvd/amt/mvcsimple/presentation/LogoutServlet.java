package ch.heigvd.amt.mvcsimple.presentation;

import javax.servlet.http.*;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Invalidate the session
        request.getSession().invalidate();

        // Redirect the user
        response.sendRedirect(request.getContextPath() + "/registration");
    }
}