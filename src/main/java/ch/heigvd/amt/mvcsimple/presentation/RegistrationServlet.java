package ch.heigvd.amt.mvcsimple.presentation;

import ch.heigvd.amt.mvcsimple.business.RegistrationManagement;
import ch.heigvd.amt.mvcsimple.services.dao.*;
import ch.heigvd.amt.mvcsimple.smtp.MailSender;

import javax.ejb.EJB;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.IOException;

/**
 * The RegistrationServlet is the Controller in the pattern. It receives HTTP requests, decides that the
 * RegistrationManagement service can provide the model. After invoking the service and obtaining the model,
 * it attaches the model to the request.
 * Finally, it finds the view capable of rendering the model (registration.jsp) and delegates the end of the work
 * to this component (by calling forwarding the request).
 */
public class RegistrationServlet extends HttpServlet {

    @EJB
    private UsersManagerLocal usersManager;

    @EJB
    private ValidationsManagerLocal validationsManager;

    @EJB
    private MailSender mailSender;

    private RegistrationManagement service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        service = new RegistrationManagement();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Identify the form sent
        String formName = request.getParameter("fFormName");

        // Handle the request
        if(formName.compareTo("SignIn") == 0){
            service.handleSignInOperation(usersManager, validationsManager, mailSender, request, response);

        } else if(formName.compareTo("LogIn") == 0) {
            service.handleLogInOperation(usersManager, validationsManager, mailSender, request, response);
        }
    }
}