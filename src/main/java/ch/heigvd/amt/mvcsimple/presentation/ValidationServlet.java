package ch.heigvd.amt.mvcsimple.presentation;

import ch.heigvd.amt.mvcsimple.services.dao.*;
import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

public class ValidationServlet extends HttpServlet {

    @EJB
    private ValidationsManagerLocal validationsManager;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        // Get the token from the url
        String token = request.getParameter("token");

        // Check the validity of the token
        long user_id;

        if((user_id = validationsManager.checkTokenValidity(token)) != 0) {

            // Validate the user
            if(validationsManager.validateUser(user_id)) {
                request.setAttribute("info_success", "Your account has been validated.");

            } else {
                request.setAttribute("info_errors", "Your account validation failed. Try again.");
            }

        } else {
            
            request.setAttribute("info_errors", "Your validation link has expired. Try to log in to get a new one.");
        }

        // Redirect the user
        request.getRequestDispatcher("/WEB-INF/pages/registration.jsp").forward(request, response);
    }
}
