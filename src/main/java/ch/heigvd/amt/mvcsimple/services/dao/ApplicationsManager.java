package ch.heigvd.amt.mvcsimple.services.dao;

import ch.heigvd.amt.mvcsimple.model.Application;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.sql.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ApplicationsManager implements ApplicationsManagerLocal {

    @Resource(lookup = "jdbc/gamification")
    private DataSource dataSource;

    private final String CREATE_APP = "INSERT INTO application (`id`, `name`, `api_key`, `api_secret`, `description`, `user_id`) VALUES (NULL, ?, ?, ?, ?, ?)";
    private final String UPDATE_APP = "UPDATE application SET name=?, description=? WHERE id =?";
    private final String REMOVE_APP = "DELETE FROM application WHERE id=?";
    private final String EXISTING_APP = "SELECT * FROM application WHERE name=? AND user_id=?";
    private final String LIST_APPLICATIONS = "SELECT * FROM application WHERE user_id=? ORDER BY name LIMIT ?,?";

    @Override
    public boolean createApplication(String name, String api_key, String api_secret, String description, long user_id) {

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(CREATE_APP);
            pstmt.setString(1, name);
            pstmt.setString(2, api_key);
            pstmt.setString(3, api_secret);
            pstmt.setString(4, description);
            pstmt.setLong(5, user_id);

            // Send the query
            pstmt.execute();
            return true;

        } catch (SQLException e) {
            Logger.getLogger(ApplicationsManager.class.getName()).log(Level.SEVERE, "An error happened while creating the application in the database.", e);
        }

        return false;
    }

    @Override
    public boolean updateApplication(long id, String name, String description, long user_id) {

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(UPDATE_APP);
            pstmt.setString(1, name);
            pstmt.setString(2, description);
            pstmt.setLong(3, id);

            // Send the query
            pstmt.execute();
            return true;

        } catch (SQLException e) {
            Logger.getLogger(ApplicationsManager.class.getName()).log(Level.SEVERE, "An error happened while creating the application in the database.", e);
        }

        return false;
    }

    @Override
    public boolean removeApplication(long app_id) {

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(REMOVE_APP);
            pstmt.setLong(1, app_id);

            // Send the query
            pstmt.execute();
            return true;

        } catch (SQLException e) {
            Logger.getLogger(ApplicationsManager.class.getName()).log(Level.SEVERE, "An error happened while removing the application from the database.", e);
            return false;
        }
    }

    @Override
    public List<Application> getApplications(long user_id, int page, int limitPerPage) {

        List<Application> applications = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(LIST_APPLICATIONS);
            pstmt.setLong(1, user_id);
            pstmt.setInt(2,(page - 1) * limitPerPage);
            pstmt.setInt(3, limitPerPage);

            // Get all user's applications from the database
            ResultSet result = pstmt.executeQuery();

            while(result.next()) {

                // Get an application
                long id = result.getLong("id");
                String name = result.getString("name");
                String api_key = result.getString("api_key");
                String api_secret = result.getString("api_secret");
                String description = result.getString("description");

                // Add the application in the list
                applications.add(new Application(id, name, api_key, api_secret, description, user_id));
            }

        } catch (SQLException e) {
            Logger.getLogger(ApplicationsManager.class.getName()).log(Level.SEVERE, "An error happened while retrieving user's applications from the database.", e);
            applications = new ArrayList<>();
        }

        return applications;
    }

    @Override
    public boolean isExistingApplication(long id, String name, long user_id, boolean isUpdate) {

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(!isUpdate ? EXISTING_APP : EXISTING_APP + " AND id !=" + id);
            pstmt.setString(1, name);
            pstmt.setLong(2, user_id);

            // Check if the application exists in the database
            ResultSet result = pstmt.executeQuery();
            return result.next();

        } catch (SQLException e) {
            Logger.getLogger(ApplicationsManager.class.getName()).log(Level.SEVERE, "An error happened while controling if the application exists for the current user.", e);
            return false;
        }
    }
}
