package ch.heigvd.amt.mvcsimple.services.dao;

import ch.heigvd.amt.mvcsimple.model.Application;
import javax.ejb.Local;
import java.util.*;

@Local
public interface  ApplicationsManagerLocal {

    /**
     * Insert an application in the database.
     *
     * @param name          Name of the application
     * @param api_key       API key of the application
     * @param api_secret    API secret of the application
     * @param description   Description of the application
     * @param user_id       ID of the owner of the application
     *
     * @return true if the application has been created, false otherwise.
     */
    boolean createApplication(String name, String api_key, String api_secret, String description, long user_id);

    /**
     * Update an application in the database.
     *
     * @param id    ID of the application
     * @param name  Name of the application
     * @param description   Description of the application
     * @param user_id Owner's ID of the application
     *
     * @return true if the application has been updated, false otherwise
     */
    boolean updateApplication(long id, String name, String description, long user_id);

    /**
     * Remove an application from the database.
     *
     * @param app_id    ID of the application.
     *
     * @return true if the application has been removed, false otherwise.
     */
    boolean removeApplication(long app_id);

    /**
     * Get all applications of a user.
     *
     * @param user_id   ID of the application owner.
     * @param page Page of the applications we want te retrieve
     * @param limitPerPage Number of applications to get per page
     *
     * @return  list of applications.
     */
    List<Application> getApplications(long user_id, int page, int limitPerPage);

    /**
     * Check if the application exists in the database for the current user.
     *
     * @param id    ID of the application
     * @param name     Name of the application.
     * @param user_id  Id of the current user.
     * @param isUpdate  true if it's an update, false otherwise
     *
     * @return true if the application exists, false otherwise
     */
    boolean isExistingApplication(long id, String name, long user_id, boolean isUpdate);
}
