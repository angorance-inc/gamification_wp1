package ch.heigvd.amt.mvcsimple.services.dao;

import ch.heigvd.amt.mvcsimple.model.User;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.sql.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class UsersManager implements UsersManagerLocal {

    @Resource(lookup = "jdbc/gamification")
    private DataSource dataSource;

    private final String CREATE_USER = "INSERT INTO user (`id`, `first_name`, `last_name`, `email`, `password`) VALUES (NULL, ?, ?, ?, ?)";
    private final String EXISTING_EMAIL = "SELECT * FROM user WHERE email=?";
    private final String EXISTING_USER = "SELECT U.id, U.first_name, U.last_name, U.email, U.is_active, U.is_verified, U.password_reseted, U.password, "
                                        + "(SELECT Count(*) FROM user_has_role UH INNER JOIN role RO ON RO.id = UH.role_id "
                                        + "WHERE RO.name = 'Administrator' AND UH.user_id = U.id) AS is_admin, "
                                        + "(SELECT Count(*) FROM application A WHERE A.user_id = U.id) AS nbrApps "
                                        + "FROM user U "
                                        + "INNER JOIN user_has_role UR ON U.id = UR.user_id "
                                        + "INNER JOIN role R ON UR.role_id = R.id "
                                        + "WHERE email=?";
    private final String UPDATE_USER = "UPDATE user SET first_name=?, last_name=? WHERE id=?";
    private final String UPDATE_PASSWORD = "UPDATE user SET password=? WHERE id=?";
    private final String RESET_PASSWORD = "UPDATE user SET password=?, password_reseted=? WHERE id=?";
    private final String LIST_USERS = "SELECT *, "
										+ "(SELECT Count(*) FROM user_has_role UH INNER JOIN role RO ON RO.id = UH.role_id "
										+ " WHERE RO.name = 'Administrator' AND UH.user_id = U.id) AS is_admin, "
										+ "(SELECT Count(*) FROM application A WHERE A.user_id = U.id) AS nbrApps "
										+ "FROM user U WHERE U.id != ? ORDER BY last_name, first_name LIMIT ?,?";
    private final String TOGGLE_ACTIVE = "UPDATE user SET is_active=? WHERE id=?";
    private final String COUNT_USERS ="SELECT COUNT(*) AS nbrUsers FROM user";
	private final String USER_BY_ID = "SELECT U.id, U.first_name, U.last_name, U.email, U.is_active, U.is_verified, U.password_reseted, U.password, "
										+ "(SELECT Count(*) FROM user_has_role UH INNER JOIN role RO ON RO.id = UH.role_id "
										+ "WHERE RO.name = 'Administrator' AND UH.user_id = U.id) AS is_admin, "
										+ "(SELECT Count(*) FROM application A WHERE A.user_id = U.id) AS nbrApps "
										+ "FROM user U "
										+ "INNER JOIN user_has_role UR ON U.id = UR.user_id "
										+ "INNER JOIN role R ON UR.role_id = R.id "
										+ "WHERE U.id=?";

    @Override
    public long createUser(String firstName, String lastName, String email, String password) {

        try (Connection connection = dataSource.getConnection()){

            if(!isExistingEmail(email)) {
                
                String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

                // Prepare the statement
                PreparedStatement pstmt = connection.prepareStatement(CREATE_USER, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, firstName);
                pstmt.setString(2, lastName);
                pstmt.setString(3, email);
                pstmt.setString(4, hashedPassword);

                // Send the query
                pstmt.execute();

                // Get key
                ResultSet key = pstmt.getGeneratedKeys();
                key.next();
                return key.getInt(1);

            } else {
                Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An account already exists with this email address");
            }

        } catch (SQLException e) {
            Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while creating an user in the database.", e);
        }

        return 0;
    }

    @Override
    public User getExistingUser(String email, String password) {

        User user = null;

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(EXISTING_USER);
            pstmt.setString(1, email);

            // Check if the user exists in the database
            ResultSet result = pstmt.executeQuery();

            if(result.next()) {
            	String hashedPassword = result.getString("password");
            	
            	if(BCrypt.checkpw(password, hashedPassword)) {
		            long id = result.getLong("id");
		            String first_name = result.getString("first_name");
		            String last_name = result.getString("last_name");
		            boolean is_active = result.getBoolean("is_active");
		            boolean is_verified = result.getBoolean("is_verified");
		            boolean password_reseted = result.getBoolean("password_reseted");
		            boolean is_admin = result.getInt("is_admin") == 1;
		            int nbrApps = result.getInt("nbrApps");
		
		            user = new User(id, first_name, last_name, email, is_active, is_verified, password_reseted, is_admin, nbrApps);
	            } else {
		            Logger.getLogger(UsersManager.class.getName()).log(Level.INFO, "Bad credentials.");
	            }
            } else {
                Logger.getLogger(UsersManager.class.getName()).log(Level.INFO, "The user doesn't exist.");
            }

        } catch (SQLException e) {
            Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while controling if user exists in the database.", e);
            user = null;
        }

        return user;
    }

    @Override
    public boolean isExistingEmail(String email) {

        try (Connection connection = dataSource.getConnection()){

            // Prepare the statement
            PreparedStatement pstmt = connection.prepareStatement(EXISTING_EMAIL);
            pstmt.setString(1, email);

            // Check if the user exists in the database
            ResultSet result = pstmt.executeQuery();
            return result.next();

        } catch (SQLException e) {
            Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while controling if the email exists.", e);
            return false;
        }
    }
	
	@Override
	public boolean updateUser(long id, String firstName, String lastName, String password) {
		
		try (Connection connection = dataSource.getConnection()){
			
			// Prepare the statement
			if(password == null) {
				PreparedStatement pstmt = connection.prepareStatement(UPDATE_USER);
				pstmt.setString(1, firstName);
				pstmt.setString(2, lastName);
				pstmt.setLong(3, id);

				// Send the query
				pstmt.execute();

			} else {
				String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

				PreparedStatement pstmt = connection.prepareStatement(UPDATE_PASSWORD);
				pstmt.setString(1, hashedPassword);
				pstmt.setLong(2, id);

				// Send the query
				pstmt.execute();
			}
			
		} catch (SQLException e) {
			Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while updating the user in the database.", e);
			
			return false;
		}

		return true;
	}
	
	@Override
	public List<User> getUsers(long user_id, int page, int limitPerPage) {
		
		List<User> users = new ArrayList<>();
		
		try (Connection connection = dataSource.getConnection()){
			
			// Prepare the statement
			PreparedStatement pstmt = connection.prepareStatement(LIST_USERS);
			pstmt.setLong(1, user_id);
			pstmt.setInt(2,(page - 1) * limitPerPage);
			pstmt.setInt(3, limitPerPage);
			
			// Get all user's applications from the database
			ResultSet result = pstmt.executeQuery();
			
			while(result.next()) {
				
				// Get a user
				long id = result.getLong("id");
				String firstName = result.getString("first_name");
				String lastName = result.getString("last_name");
				String email = result.getString("email");
				boolean is_active = result.getBoolean("is_active");
				boolean is_verified = result.getBoolean("is_verified");
				boolean password_reseted = result.getBoolean("password_reseted");
				boolean is_admin = result.getInt("is_admin") == 1;
				int nbrApps = result.getInt("nbrApps");
				
				// Add the application in the list
				users.add(new User(id, firstName, lastName, email, is_active, is_verified, password_reseted, is_admin, nbrApps));
			}
			
		} catch (SQLException e) {
			Logger.getLogger(ApplicationsManager.class.getName()).log(Level.SEVERE, "An error happened while retrieving users from the database.", e);
			users = new ArrayList<>();
		}
		
		return users;
	}
	
	@Override
	public boolean resetPassword(long id, String password, boolean password_reseted) {
		
		try (Connection connection = dataSource.getConnection()){
			
			String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
			
			// Prepare the statement
			PreparedStatement pstmt = connection.prepareStatement(RESET_PASSWORD);
			pstmt.setString(1, hashedPassword);
			pstmt.setBoolean(2, password_reseted);
			pstmt.setLong(3, id);
			
			// Send the query
			pstmt.execute();
			
		} catch (SQLException e) {
			Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while updating the user's password in the database.", e);
			
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean toggleActiveUser(long id, boolean isActive) {
  
		try (Connection connection = dataSource.getConnection()) {
			
			// Prepare the statement
			PreparedStatement pstmt = connection.prepareStatement(TOGGLE_ACTIVE);
			pstmt.setBoolean(1, isActive);
			pstmt.setLong(2, id);
			
			// Send the query
			pstmt.execute();
			
		} catch (SQLException e) {
			Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while updating the user's password in the database.", e);
			
			return false;
		}
		
		return true;
	}

	@Override
	public int getNbrUsers() {

		int nbrUsers = 0;

		try (Connection connection = dataSource.getConnection()){

			// Prepare the statement
			PreparedStatement pstmt = connection.prepareStatement(COUNT_USERS);

			// Count the users in the database
			ResultSet result = pstmt.executeQuery();

			if(result.next()) {
				nbrUsers = result.getInt("nbrUsers");

			} else {
				Logger.getLogger(UsersManager.class.getName()).log(Level.INFO, "No user in the database.");
			}

		} catch (SQLException e) {
			Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while counting the users in the database.", e);
		}

		return nbrUsers;
	}

	@Override
	public User getUserByID(long user_id) {

		User user = null;

		try (Connection connection = dataSource.getConnection()){

			// Prepare the statement
			PreparedStatement pstmt = connection.prepareStatement(USER_BY_ID);
			pstmt.setLong(1, user_id);

			// Get the user from the database
			ResultSet result = pstmt.executeQuery();

			if(result.next()) {
				String first_name = result.getString("first_name");
				String last_name = result.getString("last_name");
				String email = result.getString("email");
				boolean is_active = result.getBoolean("is_active");
				boolean is_verified = result.getBoolean("is_verified");
				boolean password_reseted = result.getBoolean("password_reseted");
				boolean is_admin = result.getInt("is_admin") == 1;
				int nbrApps = result.getInt("nbrApps");

				user = new User(user_id, first_name, last_name, email, is_active, is_verified, password_reseted, is_admin, nbrApps);

			} else {
				Logger.getLogger(UsersManager.class.getName()).log(Level.INFO, "The user doesn't exist.");
			}

		} catch (SQLException e) {
			Logger.getLogger(UsersManager.class.getName()).log(Level.SEVERE, "An error happened while getting a user from the database.", e);
			user = null;
		}

		return user;
	}
}
