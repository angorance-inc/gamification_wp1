package ch.heigvd.amt.mvcsimple.services.dao;

import ch.heigvd.amt.mvcsimple.model.User;
import javax.ejb.Local;
import java.util.List;

@Local
public interface UsersManagerLocal {

    /**
     * Insert an user in the database.
     *
     * @param firstName First name of the user
     * @param lastName  Last name of the user
     * @param email     Email of the user
     * @param password  Password of the user
     *
     * @return the id of the user.
     */
    long createUser(String firstName, String lastName, String email, String password);

    /**
     * Check if the current account exists in the database.
     *
     * @param email     Email of the account.
     * @param password  Password of the account.
     *
     * @return the user object if he's existing, null otherwise
     */
    User getExistingUser(String email, String password);

    /**
     * Check if the current email exists in the database.
     *
     * @param email     Email of the account.
     *
     * @return true if the email exists, false otherwise
     */
    boolean isExistingEmail(String email);
    
    /**
     * Update the fields of a user.
     *
     * @param id                User id
     * @param firstName         New first name of the user
     * @param lastName          New last name of the user
     * @param password          New password of the user
     *
     * @return True if the update was performed, false otherwise
     */
    boolean updateUser(long id, String firstName, String lastName, String password);
	
	/**
	 * Get a list of all users.
	 *
	 * @param user_id ID of the administrator
	 * @param page Page of the applications we want te retrieve
	 * @param limitPerPage Number of applications to get per page
	 *
	 * @return users
	 */
	List<User> getUsers(long user_id, int page, int limitPerPage);
	
	/**
	 * Update the password of the user.
	 *
	 * @param id                User id
	 * @param password          new password
	 * @param password_reseted  was the password reset by admin?
	 *
	 * @return  True if the update was performed, false otherwise
	 */
	boolean resetPassword(long id, String password, boolean password_reseted);
	
	/**
	 * Toggle the activeness of a user.
	 *
	 * @param id        User id
	 * @param isActive  Is the user active or not
	 *
	 * @return True if user was activated/deactivated successfully, false otherwise
	 */
	boolean toggleActiveUser(long id, boolean isActive);


	/**
	 * Get the total of users in the database.
	 *
	 * @return number of users
	 */
    int getNbrUsers();

	/**
	 * Get a user by ID.
	 *
	 * @param user_id	ID of the user
	 *
	 * @return user
	 */
	User getUserByID(long user_id);
}
