package ch.heigvd.amt.mvcsimple.services.dao;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.sql.*;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.logging.*;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ValidationsManager implements ValidationsManagerLocal {

    @Resource(lookup = "jdbc/gamification")
    private DataSource dataSource;

    private final String CREATE_TOKEN = "INSERT INTO validation (`user_id`, `token`, `validity`) VALUES (?, ?, ?)";
    private final String GET_TOKEN ="SELECT * FROM validation WHERE token=?";
    private final String VALIDATE_USER = "UPDATE user SET is_verified = 1 WHERE id=?";
    private final String DELETE_TOKEN = "DELETE FROM validation WHERE user_id=?;";

    @Override
    public String createValidationToken(long user_id) {

        // Generate a token
        String token = UUID.randomUUID().toString();

        try (Connection connection = dataSource.getConnection()) {

            // Delete the token of the user if exists
            PreparedStatement pstmt = connection.prepareStatement(DELETE_TOKEN);
            pstmt.setLong(1, user_id);
            pstmt.execute();

            // Create a validity of 48 hours
            Date validity = new Date(System.currentTimeMillis());

            Calendar c = Calendar.getInstance();
            c.setTime(validity);
            c.add(Calendar.DATE, 2);

            validity = new Date(c.getTimeInMillis());


            // Create a token for the user
            PreparedStatement pstmt2 = connection.prepareStatement(CREATE_TOKEN);
            pstmt2.setLong(1, user_id);
            pstmt2.setString(2, token);
            pstmt2.setDate(3, validity);

            pstmt2.execute();

        } catch (SQLException e) {
            Logger.getLogger(ValidationsManager.class.getName()).log(Level.SEVERE, "An error happened while creating the token for the user.", e);
            token = null;
        }

        return token;
    }

    @Override
    public long checkTokenValidity(String token) {
        
        long user_id = 0;

        try (Connection connection = dataSource.getConnection()){

            // Get the token from the database
            PreparedStatement pstmt = connection.prepareStatement(GET_TOKEN);
            pstmt.setString(1, token);

            ResultSet result = pstmt.executeQuery();

            if(result.next()) {

                Date validity = result.getDate("validity");

                // If the validity is up to date
                if(validity.compareTo(new Date(System.currentTimeMillis())) > 0) {
                    user_id = result.getLong("user_id");
                }

            } else {
                throw new SQLException();
            }

        } catch (SQLException e) {
            Logger.getLogger(ValidationsManager.class.getName()).log(Level.SEVERE, "An error happened while checking the token.", e);
        }
        
        return user_id;
    }

    @Override
    public boolean validateUser(long user_id) {

        try (Connection connection = dataSource.getConnection()){

            // Validate the user
            PreparedStatement pstmt = connection.prepareStatement(VALIDATE_USER);
            pstmt.setLong(1, user_id);
            pstmt.execute();

            // Delete the token of the validated user
            PreparedStatement pstmt2 = connection.prepareStatement(DELETE_TOKEN);
            pstmt2.setLong(1, user_id);
            pstmt2.execute();

            return true;

        } catch (SQLException e) {
            Logger.getLogger(ValidationsManager.class.getName()).log(Level.SEVERE, "An error happened while validating the user.", e);
            return false;
        }
    }
}
