package ch.heigvd.amt.mvcsimple.services.dao;

import javax.ejb.Local;

@Local
public interface ValidationsManagerLocal {

    /**
     * Create a validation token for a specific user.
     *
     * @param user_id   ID of the user
     *
     * @return  token generated
     */
    String createValidationToken(long user_id);

    /**
     * Check the validity of a token.
     *
     * @param token Token to check
     *
     * @return owner's ID
     */
    long checkTokenValidity(String token);

    /**
     * Validate the user, his email address has been verified.
     *
     * @param user_id   ID of the user
     *
     * @return true if the user has been validated, false otherwise
     */
    boolean validateUser(long user_id);
}
