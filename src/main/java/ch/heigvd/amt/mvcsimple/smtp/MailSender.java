package ch.heigvd.amt.mvcsimple.smtp;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * Class used to send mail with activation link to new users.
 */
@Stateless
public class MailSender {

    @Resource(name="mail/gamification")
    Session mailSession;

    /**
     * Send the activation link to the user.
     *
     * @param email Email of the user.
     * @param title Title of the email
     */
    public boolean sendMail(String email, String title, String text) {

        // Parameters of the email
        String to = email;

        // Create the message
        Message msg = new MimeMessage(mailSession);

        try {

            // Fill the message
            msg.setFrom(new InternetAddress("gamification@angorance.tech"));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setSubject(title);
            msg.setText(text);
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            // Send the message
            Transport.send(msg);

        } catch (MessagingException e) {
            e.printStackTrace();
            
            return false;
        }
        
        return true;
    }
}
