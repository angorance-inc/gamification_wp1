<%--
  Created by IntelliJ IDEA.
  User: Line
  Date: 02.11.2018
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gamification - Administration</title>
        <base href="${pageContext.request.contextPath}/">

        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="static/css/administration.css" type="text/css" rel="stylesheet" />
        <link href="static/css/global.css" type="text/css" rel="stylesheet" />
    </head>

    <body>
        <div id="topBar">
            <h3>Administrate</h3>
            <h2>GameMyApp</h2>

            <div style="display:flex; height:50%;">
                <c:if test="${user.isAdmin() == true}">
                    <form action="${pageContext.request.contextPath}/gamification">
                        <button style="color: black; height:100%;" class="btn btn-material-text"> <i class="material-icons">star_border</i> Gamification Zone</button>
                    </form>
                </c:if>
                <form  style="margin:0" method="post" action="${pageContext.request.contextPath}/logout">
                    <button  style="height:100%" id="btnlogout" type="submit" class="btn btn-danger btn-material" value="Logout"><i class="material-icons">exit_to_app</i></button>
                </form>
            </div>
        </div>

        <div id="mainContent">
            <div id="notificationZone">
                <c:if test="${not empty info_errors}">
                    <div id="info_errors" class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Error!</strong> ${info_errors}
                    </div>
                </c:if>
                <c:if test="${not empty info_success}">
                    <div id="info_success" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> ${info_success}
                    </div>
                </c:if>
            </div>

            <div class="table-responsive">
                <c:choose>
                    <c:when test="${users.size() > 0}">
                        <table id="myTable" class="display table" width="100%" >
                            <thead>
                                <tr>
                                    <th width="29%">Name</th>
                                    <th width="29%">Email</th>
                                    <th width="5%">Active</th>
                                    <th width="5%">Verified</th>
                                    <th width="10%">Password reseted</th>
                                    <th width="6%">Admin</th>
                                    <th width="8%"># of apps</th>
                                    <th width="6%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${users}" var="user">
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/gamification/${user.getId()}">${user.getLastName()} ${user.getFirstName()}</a></td>
                                    <td>${user.getEmail()}</td>
                                    <td>${user.isActive()}</td>
                                    <td>${user.isVerified()}</td>
                                    <td>${user.passwordIsReseted()}</td>
                                    <td>${user.isAdmin()}</td>
                                    <td>${user.getNbrApps()}</td>
                                    <td>
                                        <div class="action-cell">
                                            <form method="post" action="${pageContext.request.contextPath}/administration">
                                                <input type="hidden" name="fFormName" value="regenUserPassword" />
                                                <input type="hidden" class="page" name="page" value="${page}" />
                                                <input type="hidden" name="userId" value="${user.getId()}" />
                                                <input type="hidden" name="email" value="${user.getEmail()}" />
                                                <button type="submit" class="btn btnRegenPassword btn-material">
                                                    <i class='material-icons'>autorenew</i>
                                                </button>
                                            </form>
                                            <form method="post" action="${pageContext.request.contextPath}/administration">
                                                <input type="hidden" name="fFormName" value="toggleActivity" />
                                                <input type="hidden" class="page" name="page" value="${page}" />
                                                <input type="hidden" name="userId" value="${user.getId()}" />
                                                <input type="hidden" name="isActive" value="${user.isActive()}" />
                                                <c:choose>
                                                    <c:when test="${user.isActive() == true}">
                                                        <button type="submit" class="btn btnLockUser btn-material">
                                                            <i class='material-icons'>lock_open</i>
                                                        </button>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <button type="submit" class="btn btnUnlockUser btn-material">
                                                            <i class='material-icons'>lock</i>
                                                        </button>
                                                    </c:otherwise>
                                                </c:choose>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Active</th>
                                <th>Verified</th>
                                <th>Password reseted</th>
                                <th>Admin</th>
                                <th># of apps</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>

                        <c:if test="${nbrItems > limitPerPage}">
                            <div class="container">
                                <form method="post" id="DisplayItems" action="${pageContext.request.contextPath}/administration">
                                    <input type="hidden" name="fFormName" value="DisplayItems" />
                                    <input type="hidden" id="nbrItems" name="nbrItems" value="${nbrItems}" />
                                    <input type="hidden" id="page" name="page" value="${page}" />
                                    <input type="hidden" id="limitPerPage" name="limitPerPage" value="${limitPerPage}" />
                                    <nav id="pagination" style="text-align: center">
                                        <ul class="pagination justify-content-center">
                                        </ul>
                                    </nav>
                                </form>
                            </div>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <h3>No users found</h3>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="static/js/global.js"></script>

        <c:if test="${nbrItems > limitPerPage}">
            <script src="static/js/pagination.js"></script>
        </c:if>
    </body>
</html>