<%--
  Created by IntelliJ IDEA.
  User: Line
  Date: 02.11.2018
  Time: 13:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title>Gamification</title>
        <meta equiv="Content-Type" content="text/html; charset=UTF-8">
        <base href="${pageContext.request.contextPath}/">

        <link href="static/css/dialog-polyfill.css" type="text/css" rel="stylesheet" />
        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="static/css/global.css" type="text/css" rel="stylesheet" />
        <link href="static/css/gamification.css" type="text/css" rel="stylesheet" />
    </head>

    <body>
        <div id="topBar">
            <c:if test="${empty userToDisplay}">
                <div id="user-button" class="btn-material-text" onclick="openDialogue('userDial')">
                    <i class="material-icons">person</i>
                    <div>${user.getFirstName()} ${user.getLastName()}<br>(${user.getEmail()})</div>
                </div>
            </c:if>
            <h2>GameMyApp</h2>

            <div style="display:flex; height:50%;">
                <c:if test="${user.isAdmin() == true}">
                    <form action="${pageContext.request.contextPath}/administration">
                        <button style="color: black" class="btn btn-material-text"> <i class="material-icons">security</i> Administration Zone</button>
                    </form>
                </c:if>
                <form  style="margin:0" method="post" action="${pageContext.request.contextPath}/logout">
                    <button  style="height:100%" id="btnlogout" type="submit" class="btn btn-danger btn-material" value="Logout"><i class="material-icons">exit_to_app</i></button>
                </form>
            </div>
        </div>

        <div id="mainContent">
            <div id="notificationZone">
                <c:if test="${not empty info_errors}">
                    <div id="info_errors" class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Error!</strong> ${info_errors}
                    </div>
                </c:if>
                <c:if test="${not empty info_success}">
                    <div id="info_success" class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> ${info_success}
                    </div>
                </c:if>
            </div>

            <c:if test="${empty userToDisplay}">
                <div id="actionBar">
                    <button class="btn btn-success btn-material-text" value="Create" onclick="openDialogue('createDial')"> <i class="material-icons">add</i>Create</button>
                </div>
            </c:if>

            <c:if test="${not empty userToDisplay}">
                <h3>${userToDisplay.getFirstName()} ${userToDisplay.getLastName()}</h3>
            </c:if>

            <div class="table-responsive">
                <c:choose>
                    <c:when test="${applications.size() > 0}">
                        <table id="myTable" class="display table" width="100%" >
                            <thead>
                                <tr>
                                    <th width="15%">Name</th>
                                    <th width="34%">Description</th>
                                    <th width="23%">API key</th>
                                    <th width="23%">API secret</th>
                                    <c:if test="${empty userToDisplay}">
                                        <th width="5%">Actions</th>
                                    </c:if>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${applications}" var="application">
                                    <tr>
                                        <td class="oldApp_Name">${application.getName()}</td>
                                        <td class="oldApp_Description">${application.getDescription()}</td>
                                        <td class="oldApp_API_Key">
                                            <div class="action-cell">
                                                ${application.getApiKey()}
                                                <button type="submit" onclick="copyToClipboard(this)" class="btn btn-material" value="${application.getApiKey()}">
                                                    <i class='material-icons'>content_copy</i>
                                                </button>
                                            </div>
                                        <td class="oldApp_API_Secret">
                                            <div class="action-cell">
                                                ${application.getApiSecret()}
                                                <button type="submit" onclick="copyToClipboard(this)" class="btn btn-material" value="${application.getApiSecret()}">
                                                    <i class='material-icons'>content_copy</i>
                                                </button>
                                            </div>
                                        </td>
                                        <c:if test="${empty userToDisplay}">
                                            <td>
                                                <div class="action-cell">
                                                    <button type="button" class="btn btnUpdateApp btn-material" value="${application.getId()}">
                                                        <i class='material-icons'>edit</i>
                                                    </button>
                                                    <form method="post" action="${pageContext.request.contextPath}/gamification">
                                                        <input type="hidden" name="fFormName" value="RemoveApp" />
                                                        <input type="hidden" class="page" name="page" value="${page}" />
                                                        <input type="hidden" name="removeApp_id" value="${application.id}" />
                                                        <button type="submit" onclick="return confirm('Do you want to remove this application ?')" class="btn btnRemoveApp btn-material" value="${application.id}">
                                                            <i class='material-icons'>delete</i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>API key</th>
                                    <th>API secret</th>
                                    <c:if test="${empty userToDisplay}">
                                        <th>Actions</th>
                                    </c:if>
                                </tr>
                            </tfoot>
                        </table>

                        <c:if test="${nbrItems > limitPerPage}">
                            <div class="container">
                                <form method="post" id="DisplayItems" action="${pageContext.request.contextPath}/gamification<c:if test="${not empty pathInfo}">${pathInfo}</c:if>">
                                    <input type="hidden" name="fFormName" value="DisplayItems" />
                                    <input type="hidden" id="nbrItems" name="nbrItems" value="${nbrItems}" />
                                    <input type="hidden" class="page" id="page" name="page" value="${page}" />
                                    <input type="hidden" id="limitPerPage" name="limitPerPage" value="${limitPerPage}" />
                                    <nav id="pagination" style="text-align: center">
                                        <ul class="pagination justify-content-center">
                                        </ul>
                                    </nav>
                                </form>
                            </div>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <h3>No applications found</h3>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <!-- APPLICATION CREATION DIALOGUE ------------------->
        <dialog id="createDial">
            <form method="post" action="${pageContext.request.contextPath}/gamification">
                <h3 align="center">Create an app</h3>
                <input type="hidden" name="fFormName" value="CreateApp" />
                <input type="text" name="newApp_Name" value="${newApp_Name}" placeholder="Name of your application" required/><br/>
                <textarea name="newApp_Description" title="Description" cols="30" rows="10" required>${newApp_Description}</textarea><br/>
                <div>
                    <button class="btn btn-success" type="submit">Add</button>
                    <button class="btn btn-warning" type="reset" onclick="closeDialogue('createDial')"> Cancel </button>
                </div>
            </form>
        </dialog>
        <!-- FIN-------------------------------------------------------->


        <!-- APPLICATION UPDATE DIALOGUE --->
        <dialog id="updateDial">
            <form method="post" action="${pageContext.request.contextPath}/gamification">
                <h3 align="center">Update your app</h3>
                <input type="hidden" name="fFormName" value="UpdateApp" />
                <input type="hidden" class="page" name="page" value="${page}" />
                <input type="hidden" name="updateApp_Id" value="${updateApp_Id}" placeholder="ID of your application"/><br/>
                <input type="text" name="updateApp_Name" value="${updateApp_Name}" placeholder="Name of your application" required/><br/>
                <textarea name="updateApp_Description" title="Description" cols="30" rows="10" required>${updateApp_Description}</textarea><br/>
                <div>
                    <button class="btn btn-success" type="submit">Update</button>
                    <button class="btn btn-warning" type="reset" onclick="closeDialogue('updateDial')"> Cancel </button>
                </div>
            </form>
        </dialog>
        <!-- FIN-------------------------------------------------------->

        <!-- USER INFO UPDATE DIALOG --->
        <dialog id="userDial">
            <form method="post" action="${pageContext.request.contextPath}/gamification">
                <h3 align="center">Update your profile</h3>
                <div>
                    <input type="hidden" name="fFormName" value="UpdateUser" />
                    <input type="text" name="updateUser_FirstName" value="${user.getFirstName()}" placeholder="First name" required>
                    <input type="text" name="updateUser_LastName" value="${user.getLastName()}" placeholder="Last name" required>
                </div>
                <button class="btn btn-danger" type="reset" onclick="resetPassword()" style="width: 25%; min-width: fit-content; align-self: center;">Change password</button>
                <div>
                    <button class="btn btn-success" type="submit">Update</button>
                    <button class="btn" type="reset" onclick="closeDialogue('userDial')"> Cancel </button>
                 </div>
            </form>
        </dialog>
        <!-- FIN-------------------------------------------------------->


        <!-- PASSWORD RESET DIALOG  -->
       <dialog id="passwordResetDial">
                <form method="post" action="${pageContext.request.contextPath}/gamification">
                    <h3 align="center">Change your password</h3>
                    <div>
                        <input type="hidden" name="fFormName" value="UpdatePassword" />
                        <input type="password"  id="newPassword" name="updatePassword" value="${updatePassword}" placeholder="New password" required>
                        <button class="btn" type="button" onclick="togglePassword()">Show/Hide</button>
                    </div>
                    <div>
                        <button class="btn btn-success" type="submit">Confirm</button>
                        <c:if test="${user.passwordIsReseted() == false}">
                            <button class="btn"  type="reset" onclick="closeDialogue('passwordResetDial')">Cancel</button>
                        </c:if>
                     </div>
                </form>
        </dialog>

        <script src="static/js/dialog-polyfill.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="static/js/global.js"></script>
        <script src="static/js/gamification.js"></script>

        <c:if test="${nbrItems > limitPerPage}">
            <script src="static/js/pagination.js"></script>
        </c:if>

        <script>
            <c:if test="${user.passwordIsReseted() == true}">
                openDialogue('passwordResetDial');
            </c:if>
        </script>
    </body>
</html>