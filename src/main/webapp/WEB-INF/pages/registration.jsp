<%--
  Created by IntelliJ IDEA.
  User: Line
  Date: 28.09.2018
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gamification - Registration</title>
        <base href="${pageContext.request.contextPath}/">

        <link href="static/css/registration.css" type="text/css" rel="stylesheet" />
        <link href="static/css/global.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <div id="colorBand">
            <h1>GameMyApp</h1>

            <p>Make your application even more attractive by integrating some game-related feature!</p>
            <p>GameMyApp will allow you to define your own Achievements adapted to your own applications. Sign up and begin immediately to make your users farm this XP! ;)</p>
        </div>
        <div id="contentZone">

            <table>
                <tr>
                    <th>Sign In</th>
                    <th>Log In</th>
                </tr>
                <tr>
                    <td>
                        <form method="post" action="${pageContext.request.contextPath}/registration">
                            <input type="hidden" name="fFormName" value="SignIn" />
                            <input type="text" name="signIn_FirstName" value="${signIn_FirstName}" placeholder="First name"/><br/>
                            <input type="text" name="signIn_LastName" value="${signIn_LastName}" placeholder="Last name"/><br/>
                            <input type="email" name="signIn_Email" value="${signIn_Email}" placeholder="your.mail@example.com"/><br/>
                            <input type="password" name="signIn_Password" value="${signIn_Password}" placeholder="Password (min: 8 characters)" /><br/>
                            <input type="submit" value="Sign in" /><br/>
                        </form>
                    </td>
                    <td>
                        <form method="post" action="${pageContext.request.contextPath}/registration">
                            <input type="hidden" name="fFormName" value="LogIn" />
                            <input type="email" name="logIn_Email" value="${logIn_Email}" placeholder="your.mail@example.com"/><br/>
                            <input type="password" name="logIn_Password" value="${logIn_Password}" placeholder="Password"/><br/>
                            <input type="submit" value="Log in" /><br/>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul>
                            <c:forEach items="${info_errors}" var="info_error">
                                <li>${info_error}</li>
                            </c:forEach>
                        </ul>

                        <label type="text" style="color: green" name="info_success">${info_success}<label/>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>