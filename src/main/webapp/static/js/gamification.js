var allDial = document.getElementsByTagName('dialog');

for(var i = 0; i < allDial.length; ++i){
    dialogPolyfill.registerDialog(allDial[i]);
}

function resetPassword(){
    closeDialogue('userDial');
    openDialogue('passwordResetDial');
}

function copyToClipboard(element) {

    // Create a dummy input to copy the string array inside it
    var dummy = document.createElement("input");

    // Add it to the document
    document.body.appendChild(dummy);

    // Set its ID
    dummy.setAttribute("id", "dummy_id");

    // Output the array into it
    document.getElementById("dummy_id").value = element.value;

    // Select it
    dummy.select();

    // Copy its contents
    document.execCommand("copy");

    // Remove it as its not needed anymore
    document.body.removeChild(dummy);
}

function togglePassword(){
    var passInput = document.getElementById('newPassword');

    if(passInput.type === 'password'){
        passInput.type= 'text';
    } else {
        passInput.type = 'password';
    }
}

// Display application information on update pop-up
$('.btnUpdateApp').click(function () {

    // Set id
    var id = $(this).val();
    document.getElementsByName('updateApp_Id')[0].value = id;

    // Set name
    var name = $(this).closest('tr').find('.oldApp_Name').text();
    document.getElementsByName('updateApp_Name')[0].value = name;

    // Set description
    var description = $(this).closest('tr').find('.oldApp_Description').text();
    document.getElementsByName('updateApp_Description')[0].value = description;

    openDialogue('updateDial');
});